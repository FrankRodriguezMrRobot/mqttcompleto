import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { BACKDROP } from '@ionic/core/dist/types/utils/overlays';
import { observable} from 'rxjs';
import {Paho} from 'ng2-mqtt/mqttws31';

declare var mqtt: any;


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  private client: any = null;
  public mensaje: string = '';
  foco1=false;

  constructor() {
    this.client = mqtt.connect('', {
      protocol: 'wss',
      host: 'soldier.cloudmqtt.com',
      port: 37997,
      path: '/mqtt',
      username: 'Prueba_mqtt',
      password: '0123456'
    });
    
    this.client.subscribe('ensayo1');
    this.client.on('message', (topic,message)=> {
      let mess = new TextDecoder("utf-8").decode(message);
      if(mess==="ENCENDIDO"){
        console.warn("DENTRO A ENCENDIDO");
        this.foco1=true;
      }
      if(mess==="APAGADO"){
        console.warn("DENTRO A APAGADO");
        this.foco1=false;
      }
      console.log(mess)
    })
    this.client.on('connect', (res) => {
      console.warn("CONNECT SUCCESS: ", res);
    })
  }
  sendMessage(mensaje) {
    this.client.publish('ensayo1', mensaje);
  }

  /////////////////////////////////
  foco1change(){
    console.info("FOCO1: ", this.foco1);
    if(this.foco1){
      this.client.publish('ensayo1','ENCENDIDO');
    }else{
      this.client.publish('ensayo1','APAGADO');
    }
    
  }

}